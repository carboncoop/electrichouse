from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Any, Dict, List, Tuple, Callable, Sequence
from datetime import datetime, timedelta, timezone, time, date
from math import pow
import logging
import os

from numpy import polyfit
import pandas as pd

from electrichouse.components import BaseComponent


@dataclass
class EVChargerInputParams:
    ev_charger_kW: float
    ev_battery_size_kWh: float


@dataclass
class EVChargerComponent(BaseComponent):

    input_params: EVChargerInputParams

    # ev weekly connected/disconnected schedule
    # expressed as start time and pandas.Period-compatible
    # in which freq string reflecting when
    # EV disconnected and energy used during
    # this time away from EV charger. Assumed connected and
    # available for charging at all other times.
    _ev_weekly_schedule = {
        0: [
            (time(9, 0, 0), "9H", 9),
        ],
        1: [
            (time(9, 0, 0), "9H", 9),
        ],
        2: [
            (time(9, 0, 0), "9H", 9),
        ],
        3: [
            (time(9, 0, 0), "9H", 9),
        ],
        4: [
            (time(9, 0, 0), "9H", 9),
        ],
        5: [
            (time(9, 0, 0), "9H", 9),
        ],
        6: [
            (time(9, 0, 0), "9H", 9),
        ],
    }

    def _get_connected_and_energy_used(self, timestamp: datetime) -> Tuple[bool, float]:
        """
        This function determines if EV is connected or not and if not how much energy
        it is using elsewhere using the partial weekly schedule from above.
        """

        for time_start, duration, energy_used in self._ev_weekly_schedule[
            timestamp.weekday()
        ]:
            p = pd.Period(
                freq=duration,
                year=timestamp.year,
                month=timestamp.month,
                day=timestamp.day,
                hour=time_start.hour,
                minute=time_start.hour,
            )

            if (
                p.start_time.tz_localize(tz="UTC")
                <= timestamp
                < p.end_time.tz_localize(tz="UTC")
            ):
                return False, (energy_used / (p.hour * 2))
        return True, 0.0

    def __post_init__(self):
        self._ev_charger_kW = self.input_params.ev_charger_kW
        self._ev_battery_size_kWh = self.input_params.ev_battery_size_kWh
        self._ev_weekly_schedule_next_slot = 0

        if self._ev_charger_kW < 0.0:
            raise RuntimeError(
                "Battery max charging power must be expressed as a positive number greater than zero."
            )

        if self._ev_battery_size_kWh < 0.0:
            raise RuntimeError(
                "EV battery size must be a positive number greater than zero."
            )

        # start with full battery
        self._ev_battery_level_kWh = self._ev_battery_size_kWh

    def dispatch(self, timestamp: datetime, control_params: Dict[str, Any]) -> dict:
        ev_charger_enabled = control_params["ev_charger_enabled"]

        connected, energy_used = self._get_connected_and_energy_used(timestamp)

        self._ev_battery_level_kWh -= energy_used

        if self._ev_battery_level_kWh < 0:
            logging.warning(
                "EV battery level is zero. Hopefully you were able to charge your car somewhere else!"
            )
            self._ev_battery_level_kWh = 0.0

        ev_final_battery_level_kWh = self._ev_battery_level_kWh
        ev_charging_session_kWh = 0.0
        if connected and ev_charger_enabled:
            ev_final_battery_level_kWh += self._ev_charger_kW / 2.0
            ev_charging_session_kWh = self._ev_charger_kW / 2.0
            if ev_final_battery_level_kWh > self._ev_battery_size_kWh:
                ev_final_battery_level_kWh = self._ev_battery_size_kWh
                ev_charging_session_kWh = (
                    ev_final_battery_level_kWh - self._ev_battery_level_kWh
                )
            self._ev_battery_level_kWh = ev_final_battery_level_kWh

        return {
            "battery_level_kWh": self._ev_battery_level_kWh,
            "charging_session_kWh": ev_charging_session_kWh,
            "connected": connected,
            "energy_used_kWh": energy_used,
            "charging_rate_kW": ev_charging_session_kWh / 2.0,
        }
