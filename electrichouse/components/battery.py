from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Any, Dict, List, Tuple, Callable, Sequence
from datetime import datetime, timedelta, timezone, time, date
from math import pow
import logging
import os

from numpy import polyfit
import pandas as pd

from electrichouse.components import BaseComponent


@dataclass
class BatteryInputParams:
    max_charge_kW: float
    max_discharge_kW: float
    max_storage_kWh: float


@dataclass
class BatteryComponent(BaseComponent):

    input_params: BatteryInputParams

    def __post_init__(self):
        self._max_charge_kW = self.input_params.max_charge_kW
        self._max_discharge_kW = self.input_params.max_discharge_kW
        self._max_storage_kWh = self.input_params.max_storage_kWh

        if self._max_charge_kW < 0.0:
            raise RuntimeError(
                "Battery max charging power must be expressed as a positive number greater than zero."
            )

        if self._max_discharge_kW < 0.0:
            raise RuntimeError(
                "Battery max discharging power must be expressed as a positive number greater than zero."
            )

        if self._max_storage_kWh < 0.0:
            raise RuntimeError(
                "Battery max storage capacity must be expressed as a positive number greater than zero."
            )

        # start with full battery
        self._stored_kWh = self._max_storage_kWh

    def dispatch(self, timestamp: datetime, control_params: Dict[str, Any]) -> dict:
        requested_power_kW = control_params["requested_power_kW"]

        # start by assuming supplied power is equal to requested power
        supplied_power_kW = requested_power_kW

        # test if requested power exceeds the max (dis)charging power
        # note here wrt signs - positive kW is power being supplied to house energy bus
        # ... so a request to charge battery is negative kW
        if -requested_power_kW > self._max_charge_kW:
            supplied_power_kW = -self._max_charge_kW

        elif requested_power_kW > self._max_discharge_kW:
            supplied_power_kW = self._max_discharge_kW

        # for given supplied power level determine if this either exceeds max storage or drains battery below zero
        # if it does modify further
        temp_storage_level_kWh = self._stored_kWh - supplied_power_kW / 2.0

        if temp_storage_level_kWh > self._max_storage_kWh:
            supplied_power_kW = -(self._max_storage_kWh - self._stored_kWh) * 2

        if temp_storage_level_kWh < 0.0:
            supplied_power_kW = 2 * self._stored_kWh

        self._stored_kWh -= supplied_power_kW / 2

        return {"supplied_power_kW": supplied_power_kW, "stored_kWh": self._stored_kWh}
