from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Any, Dict, List, Tuple, Callable, Sequence
from datetime import datetime, timedelta, timezone, time, date
from math import pow
import logging
import os

from numpy import polyfit
import pandas as pd

from electrichouse.components import BaseComponent


@dataclass
class HeatPumpInputParams:
    max_heat_output_kw: float
    max_modulation: float
    building_heat_loss_coefficient_Q_kW_K: float


@dataclass
class HeatPumpComponent(BaseComponent):

    input_params: HeatPumpInputParams

    # Air temp versus COP at 45 flow temp taken from ecodan spec sheets
    _air_temp_versus_cop_points_45 = [
        (-15, 2.24, 1.13),
        (-10, 2.74, 1.79),
        (-7, 3.04, 2.01),
        (2, 2.8, 2.61),
        (7, 3.6, 3.46),
        (12, 4.06, 4.09),
        (15, 4.34, 4.47),
        (20, 4.8, 5.1),
    ]

    # Air temp versus COP at 55 flow temp taken from ecodan spec sheets
    _air_temp_versus_cop_points_55 = [
        (-15, 0, 0),
        (-10, 0, 0),
        (-7, 2.8, 1.34),
        (2, 2.8, 1.8),
        (7, 3.6, 2.35),
        (12, 4.06, 2.74),
        (15, 4.34, 2.98),
        (20, 4.8, 3.37),
    ]

    def __post_init__(self):

        self._cop_curve_45, self._cap_curve_45 = HeatPumpComponent.gen_cop_cap_curve_eq(
            self._air_temp_versus_cop_points_45
        )
        self._cop_curve_55, self._cap_curve_55 = HeatPumpComponent.gen_cop_cap_curve_eq(
            self._air_temp_versus_cop_points_55
        )

        self._max_heat_output_kw = self.input_params.max_heat_output_kw
        max_heat_output_kw = self._max_heat_output_kw
        if max_heat_output_kw < 0:
            raise ValueError(
                "Max heat pump output must be greater than or equal to zero."
            )

        self._max_modulation = self.input_params.max_modulation
        max_modulation = self._max_modulation
        if max_modulation > 1.0 or max_modulation < 0.0:

            raise ValueError(
                "Max heat pump modulation must be a fraction between 0 and 1."
            )

        self._Q = float(self.input_params.building_heat_loss_coefficient_Q_kW_K)

        if self._Q <= 0.0:
            raise ValueError(
                "Q must be greater than zero (it must take some energy input to maintain a temp difference!)."
            )

        if (
            max_heat_output_kw * self._cap_curve_55(-15)
            < max_modulation * max_heat_output_kw
        ):
            logging.warning(
                f"The minimum modulated output {max_heat_output_kw*self._cap_curve_55(-15)}kW is lower then the lowest modulated output at -15C which is probably unphysical {self._max_modulation*self._max_heat_output_kw}. You should consider adjusting the modulation factor."
            )

    def dispatch(self, timestamp: datetime, control_params: Dict[str, Any]) -> dict:
        room_setpoint_temperature_C = control_params["room_setpoint_temperature_C"]
        outdoor_air_temperature_C = control_params["outdoor_air_temperature_C"]
        dhw_supply_active = control_params["dhw_supply_active"]

        Q = self._Q
        max_heat_output_kw = self._max_heat_output_kw

        # If the heat pump is supplying DHW use the higher temperature cop/cap curves
        if dhw_supply_active:
            cop = self._cop_curve_55
            cap = self._cap_curve_55
        else:
            cop = self._cop_curve_45
            cap = self._cap_curve_45

        # MAIN EQUATION -> this relates heat pump electrical power input required to
        # the desired indoor room setpoint temperature
        heat_pump_power_kW = (
            Q
            * (room_setpoint_temperature_C - outdoor_air_temperature_C)
            / cop(outdoor_air_temperature_C)
        )

        # Initially set room temperature to setpoint, this can be changed below
        room_temperature = room_setpoint_temperature_C

        # The below conditions modify the output from equation above based on various factors...
        heat_demand_met = True

        # If required heat pump power is less than zero it means the outdoor air temp is higher than the setpoint so the heat pump does not provide heat.
        if heat_pump_power_kW < 0:
            heat_pump_power_kW = 0.0

            room_temperature = outdoor_air_temperature_C

        # If the required heat pump power exceeds the max heat pump output (modified by the cap curve) then the heat pump cannot meet the heat demand. In this case log a warning but continue the model run setting heat_demand_met to False.
        if heat_pump_power_kW * cop(
            outdoor_air_temperature_C
        ) > max_heat_output_kw * cap(outdoor_air_temperature_C):

            heat_demand_met = False

            heat_pump_power_kW = (
                max_heat_output_kw * cap(outdoor_air_temperature_C)
            ) / cop(outdoor_air_temperature_C)

            room_temperature = outdoor_air_temperature_C + (
                (heat_pump_power_kW * cop(outdoor_air_temperature_C)) / Q
            )
            logging.warning(
                f"{timestamp}: Heat pump not able to meet heat demand {heat_pump_power_kW}kW at set point temperature {room_setpoint_temperature_C}C for outdoor air temperature {outdoor_air_temperature_C} and Q value {Q}kW/K."
            )

        # If heat pump output power required is below the minimum capacity of heat pump then heat will still supply heat but will cycle more to maintain setpoint. As a first approximation we assume the heat pump power required is equal to the output at minimum modulation but it could be much greater than this. In these marginal cases some control would not result in the heat pump running at all or not until the temperature has fallen back down the hysteresis curve.
        if (
            heat_pump_power_kW * cop(outdoor_air_temperature_C)
            < max_heat_output_kw * self._max_modulation
        ):
            heat_pump_power_kW = (
                max_heat_output_kw
                * self._max_modulation
                / cop(outdoor_air_temperature_C)
            )

            logging.warning(
                f"{timestamp}: Heat demand {heat_pump_power_kW}kW is below min heat pump output {max_heat_output_kw*self._max_modulation} for outdoor air temperature {outdoor_air_temperature_C} and Q value {Q}kW/K."
            )

        # Determine how much heat pump heat output maybe available for DHW heating (space heating assumed to have priority). Whether this is actually used will be determined by the controller.
        dhw_heat_available_kW = max_heat_output_kw * cap(
            outdoor_air_temperature_C
        ) - heat_pump_power_kW * cop(outdoor_air_temperature_C)
        if dhw_heat_available_kW < 0.0:
            dhw_heat_available_kW = 0.0

        # The following are checks to ensure output is physical and there are no issues with model.
        if room_temperature < outdoor_air_temperature_C:
            raise RuntimeError("Indoor temp cannot be less than outdoor temp here.")

        if heat_demand_met and room_temperature < room_setpoint_temperature_C:
            raise RuntimeError(
                "Heat demand has been flagged as met but room temperature is below the setpoint temperature"
            )

        return {
            "room_temperature_C": room_temperature,
            "power_usage_kW": -heat_pump_power_kW,
            "outdoor_air_temp_C": outdoor_air_temperature_C,
            "heat_demand_kW": heat_pump_power_kW * cop(outdoor_air_temperature_C),
            "cop": cop(outdoor_air_temperature_C),
            "heat_demand_met": heat_demand_met,
            "dhw_heat_available_kW": dhw_heat_available_kW,
        }

    @classmethod
    def gen_cop_cap_curve_eq(
        cls, air_temp_versus_cop_points: List[Tuple]
    ) -> Tuple[Callable[[float], float], Callable[[float], float]]:
        """
        This generates functions which relate outdoor air temperature
        to the COP and maximum heat capacity of the heat pump using
        data from manufacturer spreadsheets passed as arguments and
        then fitting polynomial curves to these points.

        """

        df = pd.DataFrame(
            air_temp_versus_cop_points,
            columns=["outdoor_air_temp_C", "heat pump capacity", "cop"],
        )

        # We just normalise the capacity curve here and scale to
        # the max heat pump capacity as required but
        # heat pumps with different capacities will in reality have
        # different scaled capacity - temperature curves.
        df["heat pump capacity norm"] = (
            df["heat pump capacity"] / df["heat pump capacity"].max()
        )

        # TODO: see if new numpy.polynomial interface can improve all this ...
        def __gen_coefs(x: float, y: float, order: int = 4) -> Tuple:
            """
            Fit curve to spec sheet data
            using numpy and return coefficients
            at order specified.
            """

            coef = polyfit(x, y, order)
            return tuple(coef)

        def __curve_eq(x: float, coef: Tuple) -> float:
            y = 0.0
            n = len(coef) - 1
            for power, coefi in enumerate(coef):
                y += pow(x, n - power) * coefi
            return y

        cop_coef = __gen_coefs(df["outdoor_air_temp_C"], df["cop"])

        def cop_curve(outdoor_air_temp_C: float) -> float:
            res = __curve_eq(outdoor_air_temp_C, cop_coef)
            if res < 0.0:
                return 0.0
            else:
                return res

        cap_coef = __gen_coefs(df["outdoor_air_temp_C"], df["heat pump capacity norm"])

        def cap_curve(outdoor_air_temp_C: float) -> float:
            res = __curve_eq(outdoor_air_temp_C, cap_coef)
            if res < 0.0:
                return 0.0
            else:
                return res

        return cop_curve, cap_curve


@dataclass
class ThermostatDHWInputParams:
    immersion_rating_kW: float
    max_heat_storage_kWh: float
    thermostat_level: float


@dataclass
class ThermostatDHWComponent(BaseComponent):

    input_params: ThermostatDHWInputParams

    def __post_init__(self):
        self._dhw_immersion_kW = self.input_params.immersion_rating_kW

        self._dhw_max_storage_kWh = self.input_params.max_heat_storage_kWh

        self._thermostat_level = self.input_params.thermostat_level

        # start with a full tank
        self._dhw_stored_kWh = self._dhw_max_storage_kWh

    def thermostat_signal(self, heat_demand_request_kW: float):
        if (
            self._dhw_max_storage_kWh * self._thermostat_level
            > self._dhw_stored_kWh - heat_demand_request_kW / 2.0
        ):
            return True
        else:
            return False

    def state(self):
        return {
            "dhw_thermostat_heat_demand_level_kW": 2.0
            * (
                self._dhw_stored_kWh
                - self._dhw_max_storage_kWh * self._thermostat_level
            )
        }

    def dispatch(self, timestamp: datetime, control_params: Dict[str, Any]) -> dict:

        max_external_heat_available_kW = control_params[
            "max_external_heat_available_kW"
        ]
        heat_demand_request_kW = control_params["heat_demand_request_kw"]

        dhw_electrical_power_used_kW = 0.0
        dhw_external_heat_used_kW = 0.0

        # if heat demand takes DHW tank below thermostat level then try to top up with external heat and maybe immersion ...
        if self.thermostat_signal(heat_demand_request_kW):

            # if external heat available does not restore DHW tank beyond thermostat level then top up with immersion
            if (
                self._dhw_max_storage_kWh * self._thermostat_level
                > self._dhw_stored_kWh
                - heat_demand_request_kW / 2.0
                + max_external_heat_available_kW / 2.0
            ):
                dhw_external_heat_used_kW = max_external_heat_available_kW
                # if immersion top up goes above max storage then limit power use to that ...
                if (
                    self._dhw_stored_kWh + self._dhw_immersion_kW / 2
                    > self._dhw_max_storage_kWh
                ):
                    dhw_electrical_power_used_kW = (
                        self._dhw_max_storage_kWh - self._dhw_stored_kWh
                    ) * 2.0
                # ... else use all available power
                else:
                    dhw_electrical_power_used_kW = self._dhw_immersion_kW

            # ...else if external heat takes tank above max storage then limit power use to what is required
            elif (
                self._dhw_max_storage_kWh
                < self._dhw_stored_kWh
                - heat_demand_request_kW / 2.0
                + max_external_heat_available_kW / 2.0
            ):
                dhw_external_heat_used_kW = (
                    self._dhw_max_storage_kWh - self._dhw_stored_kWh
                ) * 2.0
            # ... else use all available external heat.
            else:
                dhw_external_heat_used_kW = max_external_heat_available_kW

        # now determine what new DHW storage level is
        temp_storage_level = (
            self._dhw_stored_kWh
            + (
                dhw_electrical_power_used_kW
                + dhw_external_heat_used_kW
                - heat_demand_request_kW
            )
            / 2
        )

        # If DHW is below zero then not all heat demand can be met and the storage level will end up as zero.
        if temp_storage_level < 0.0:

            heat_demand_supplied_kW = (
                self._dhw_stored_kWh * 2.0
                + dhw_electrical_power_used_kW
                + dhw_external_heat_used_kW
            )

            logging.warning(
                f"DHW demand not met! {heat_demand_request_kW}kW was requested but only {heat_demand_supplied_kW}kW could be supplied!"
            )

            self._dhw_stored_kWh = 0.0

        else:
            heat_demand_supplied_kW = heat_demand_request_kW

            self._dhw_stored_kWh = temp_storage_level

        # a few checks
        if dhw_external_heat_used_kW > max_external_heat_available_kW:
            raise RuntimeError("External heat used is greater than heat available.")

        if dhw_external_heat_used_kW < 0.0:
            raise RuntimeError("External heat used is less than zero!")

        if dhw_electrical_power_used_kW < 0.0:
            raise RuntimeError("Electrical power used is less than zero!")

        if dhw_electrical_power_used_kW > self._dhw_immersion_kW:
            raise RuntimeError(
                "Electrical power used is greater than immersion rating!"
            )

        if self._dhw_stored_kWh > self._dhw_max_storage_kWh:
            raise RuntimeError(
                "Final DHW storage level is greater than max storage level!"
            )

        if self._dhw_stored_kWh < 0:
            raise RuntimeError("DHW storage level is less than zero!")

        return {
            "external_heat_used_kW": dhw_external_heat_used_kW,
            "electrical_power_used_kW": dhw_electrical_power_used_kW,
            "heat_demand_supplied_kW": heat_demand_supplied_kW,
            "stored_kWh": self._dhw_stored_kWh,
        }
