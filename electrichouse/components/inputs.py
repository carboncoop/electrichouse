from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Any, Dict, List, Tuple, Callable, Sequence
from datetime import datetime, timedelta, timezone, time, date
from math import pow
from pathlib import Path
import logging
import os

from numpy import polyfit
import pandas as pd

from electrichouse.components import BaseComponent


@dataclass
class TempAndPVInputParams:
    weather_data_filepath: Path
    weather_data_year: int
    pv_power_kW: float
    time_interval: timedelta = timedelta(minutes=30)


@dataclass
class TempAndPVComponent(BaseComponent):
    input_params: TempAndPVInputParams

    @classmethod
    def get_pvgis_hourly_data(cls, latitude, longitude, surface_tilt, surface_azimuth):
        try:
            import pvlib
        except ModuleNotFoundError:
            raise ModuleNotFoundError("pvlib is an optional dependency.")
        data, _, _ = pvlib.iotools.get_pvgis_hourly(
            latitude,
            longitude,
            surface_tilt=surface_tilt,
            surface_azimuth=surface_azimuth,
            pvcalculation=True,
            peakpower=1.0,
        )
        return data

    def __post_init__(self):

        # Load PVGIS hourly data from CSV
        # Use get_pvgis_hourly_data to get databb
        data = pd.read_csv(
            self.input_params.weather_data_filepath,
            infer_datetime_format=True,
            index_col="time",
        )
        data.index = pd.DatetimeIndex(data.index)

        # need to reindex result as offset 11 minutes for some reason (?)
        data.index = data.index - timedelta(minutes=11)

        # can resample ... but should you!?
        data = data.resample(self.input_params.time_interval).interpolate()

        # convert W to kW
        self.__pv_time_series = data["P"] * self.input_params.pv_power_kW / 1000
        #
        self.__temp_time_series = data["temp_air"]

    def dispatch(self, timestamp: datetime, control_params: Dict[str, Any]) -> dict:
        timestamp = timestamp.replace(year=self.input_params.weather_data_year)

        return {
            "pv_generation_kW": float(self.__pv_time_series.loc[timestamp]),
            "outdoor_temp_C": float(self.__temp_time_series.loc[timestamp]),
        }


@dataclass
class HouseInflexibleLoadInputParams:
    data_csv_filepath: Path


@dataclass
class HouseInflexibleLoadComponent(BaseComponent):

    input_params: HouseInflexibleLoadInputParams

    def __post_init__(self):
        load = pd.read_csv(
            self.input_params.data_csv_filepath,
            infer_datetime_format=True,
            parse_dates=True,
            index_col=0,
        )
        self.__load = load

    def dispatch(self, timestamp: datetime, control_params: Dict[str, Any]) -> dict:
        # just use same half hour in any year
        timestamp = timestamp.replace(year=2021)
        return {"usage_kW": -float(self.__load.loc[timestamp])}
