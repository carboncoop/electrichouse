from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Any, Dict, List, Tuple, Callable, Sequence
from datetime import datetime, timedelta, timezone, time, date
from math import pow
import logging
from pathlib import Path
import os

from numpy import polyfit
import pandas as pd

from electrichouse.components import BaseComponent


@dataclass
class GridInputParams:
    min_kw: float
    max_kw: float


@dataclass
class GridComponent(BaseComponent):

    input_params: GridInputParams

    def __post_init__(self):
        grid_min_kw = self.input_params.min_kw
        grid_max_kw = self.input_params.max_kw

        if grid_max_kw < 0:
            raise ValueError("Grid import must be positive definite/greater than zero.")
        if grid_min_kw > 0:
            raise ValueError("Grid export must be negative/less than zero.")

    def dispatch(self, timestamp: datetime, control_params: Dict[str, Any]) -> dict:
        grid_requested_kw = control_params["grid_requested_kw"]
        grid_min_kw = self.input_params.min_kw
        grid_max_kw = self.input_params.max_kw

        if grid_min_kw < grid_requested_kw and grid_requested_kw < grid_max_kw:
            grid_kw = grid_requested_kw
        elif grid_min_kw > grid_requested_kw:
            raise RuntimeError("Requested grid export capacity has exceeded limit!")
        elif grid_max_kw < grid_requested_kw:
            raise RuntimeError("Requested grid import capacity has exceeded limit!")

        return {"supplied_kW": grid_requested_kw}


@dataclass
class OctopusAgileTariffInputParams:
    agile_prices_csv_filepath: Path
    agile_outgoing_prices_csv_filepath: Path


@dataclass
class OctopusAgileTariffComponent(BaseComponent):
    input_params: OctopusAgileTariffInputParams

    def __post_init__(self):
        #
        agile_prices = pd.read_csv(
            self.input_params.agile_prices_csv_filepath,
            names=[
                "datetime",
                "time",
                "region code",
                "region name",
                "price_pence_per_kwh",
            ],
            index_col=0,
            parse_dates=True,
            header=None,
            infer_datetime_format=True,
        )
        self.__agile_prices = agile_prices["price_pence_per_kwh"] / 100

        #
        agile_outgoing_prices = pd.read_csv(
            self.input_params.agile_outgoing_prices_csv_filepath,
            names=[
                "datetime",
                "time",
                "region code",
                "region name",
                "price_pence_per_kwh",
            ],
            index_col=0,
            parse_dates=True,
            header=None,
            infer_datetime_format=True,
        )
        self.__agile_outgoing_prices = (
            agile_outgoing_prices["price_pence_per_kwh"] / 100
        )

    def dispatch(self, timestamp: datetime, control_params: Dict[str, Any]) -> dict:

        return {
            "grid_import_price_pounds_per_kWh": float(
                self.__agile_prices.loc[timestamp]
            ),
            "grid_export_price_pounds_per_kWh": float(
                self.__agile_outgoing_prices.loc[timestamp]
            ),
        }

    def import_prices_range(self, start: datetime, end: datetime) -> pd.DataFrame:
        return self.__agile_prices.loc[start:end]  # type: ignore


@dataclass
class SingleRateTariffInputParams:
    grid_import_price_pounds_per_kWh: float
    grid_export_price_pounds_per_kWh: float


@dataclass
class SingleRateTariffComponent(BaseComponent):
    input_params: SingleRateTariffInputParams

    def dispatch(self, timestamp: datetime, control_params: Dict[str, Any]) -> dict:
        return {
            "grid_import_price_pounds_per_kWh": self.input_params.grid_import_price_pounds_per_kWh,
            "grid_export_price_pounds_per_kWh": self.input_params.grid_export_price_pounds_per_kWh,
        }
