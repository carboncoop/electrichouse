from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Any, Dict, List, Tuple, Callable, Sequence
from datetime import datetime, timedelta, timezone, time, date
from math import pow
import logging
import os

from numpy import polyfit
import pandas as pd


@dataclass
class BaseComponent(ABC):
    """
    Prototype for Component.
    """

    @abstractmethod
    def dispatch(
        self, timestamp: datetime, control_params: Dict[str, Any]
    ) -> Dict[str, Any]:
        """
        This method dispatches the component at each time step and needs to be implemented in a Component subclass.
        """
        raise NotImplementedError

    def state(self) -> Dict[str, Any]:
        """
        This method returns the current state of the component.
        """
        return {}
