from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Any, Dict, List, Tuple, Callable, Sequence
from datetime import datetime, timedelta, timezone, time, date
from math import pow
import logging
import os
from collections import OrderedDict
from pathlib import Path

from numpy import polyfit
import pandas as pd

from electrichouse.components.inputs import (
    TempAndPVComponent,
    HouseInflexibleLoadComponent,
    TempAndPVInputParams,
    HouseInflexibleLoadInputParams,
)
from electrichouse.components.grid import (
    SingleRateTariffComponent,
    OctopusAgileTariffComponent,
    GridComponent,
    SingleRateTariffInputParams,
    OctopusAgileTariffInputParams,
    GridInputParams,
)
from electrichouse.components.battery import BatteryComponent, BatteryInputParams
from electrichouse.components.heating import (
    HeatPumpComponent,
    HeatPumpInputParams,
    ThermostatDHWComponent,
    ThermostatDHWInputParams,
)
from electrichouse.components.ev import EVChargerComponent, EVChargerInputParams


@dataclass
class BaseController(ABC):
    """
    Prototype for Controller.

    setup_components and _time_step should be implemented in sub-classes.
    """

    start_time: datetime
    end_time: datetime
    input_params: Dict[str, Any]
    control_params_init: Dict[str, Any]
    _timestamp: datetime = field(init=False)
    time_interval: timedelta

    @abstractmethod
    def __post_init__(self):
        self._timestamp = self.start_time
        self._components = {}

    @abstractmethod
    def _time_step(self) -> Dict:
        """
        This method is used to time step the model based on the plan.
        It is expected that the combined control and output params are returned.
        This method should be implemented in the InputParams.
        """
        raise NotImplementedError

    def __iter__(self):
        return self

    def __next__(self):
        if self._timestamp < self.end_time:
            res = self._time_step()
            self._timestamp = self._timestamp + self.time_interval
            return res
        else:
            raise StopIteration()


# This is a step function which does nothing provided for convenience
do_nothing_step_function: Callable[[Any], dict] = lambda **x: {}


@dataclass
class PlanController(BaseController):

    """
    This is a simple controller which can dispatch most components according to a 'plan'
    which is an ordered dictionary of inputs / components / step functions.

    The components are dispatched in order and passed the output of a 'step function' running
    on the outputs of previously run components. This can be used to initialise certain
    control parameters which may be dependent on the results of dispatching other components.

    Most control schemes can be represented by such a 'plan' given a suitable set of 'step functions'.

    The alternative is to implement your own _time_step method.
    """

    plan: OrderedDict[str, Any]

    def __post_init__(self):
        self._timestamp = self.start_time
        self._components = {}

        for component_name, component in self.plan.items():
            self._components[component_name] = (
                component["class"](input_params=component["input_params"]),
                component["step_function"],
            )

    def _time_step(self) -> Dict:

        outputs = {}
        for component_name, (component, sf) in self._components.items():
            outputs.update(component.state())

        for component_name, (component, sf) in self._components.items():
            output = component.dispatch(
                self._timestamp,
                sf(**self.input_params | outputs),
            )

            output = {component_name + "_" + k: v for k, v in output.items()}

            outputs.update(output)

        return {"timestamp": str(self._timestamp)} | outputs


@dataclass
class HeatPumpDSRController(BaseController):

    """
    This is a controller which can be used to
    analyse the use of heat pump for DSR by
    adjusting setpoint temperature based on a schedule.
    """

    def __post_init__(self):
        self._dsr_schedule: dict = {}
        self._n_dsr_intervals = self.input_params["n_dsr_intervals"]
        self._room_setpoint_temperature_flex_C = self.input_params[
            "room_setpoint_temperature_flex_C"
        ]
        self._room_setpoint_temperature_C = self.input_params[
            "room_setpoint_temperature_C"
        ]

        if self._n_dsr_intervals < 0:
            raise RuntimeError("n_dsr_intervals must be positive integer")

        if self._n_dsr_intervals > 48:
            raise RuntimeError(
                "n_dsr_intervals must be less than or equal to 48 intervals (in day)"
            )

        self._timestamp = self.start_time
        self._components = {}

        self._components["PVandTemp"] = TempAndPVComponent(
            input_params=TempAndPVInputParams(
                weather_data_filepath=Path(
                    f"{os.path.abspath('')}/../data/weather_data.csv"
                ),
                weather_data_year=2014,
                pv_power_kW=3,
            ),
        )

        self._components["Grid"] = GridComponent(
            input_params=GridInputParams(
                min_kw=-3.68,
                max_kw=23,
            ),
        )

        self._components["HouseLoad"] = HouseInflexibleLoadComponent(
            input_params=HouseInflexibleLoadInputParams(
                data_csv_filepath=Path(f"{os.path.abspath('')}/../data/loadm.csv"),
            ),
        )

        self._components["ElecPrice"] = OctopusAgileTariffComponent(
            input_params=OctopusAgileTariffInputParams(
                agile_prices_csv_filepath=f"{os.path.abspath('')}/../data/agile.csv",
                agile_outgoing_prices_csv_filepath=f"{os.path.abspath('')}/../data/agileoutgoing.csv",
            )
        )

        self._components["HeatPump"] = HeatPumpComponent(
            input_params=HeatPumpInputParams(
                max_modulation=0.0,
                max_heat_output_kw=8,
                building_heat_loss_coefficient_Q_kW_K=0.2,
            ),
        )

    def _time_step(self):

        # get grid prices
        grid_price = self._components["ElecPrice"].dispatch(self._timestamp, {})

        # if DSR schedule doesnt exist for this day create it
        # schedule is cheapest N time intervals
        if self._timestamp.date() not in self._dsr_schedule:
            self._dsr_schedule[self._timestamp.date()] = (
                self._components["ElecPrice"]
                .import_prices_range(
                    start=datetime(
                        year=self._timestamp.year,
                        month=self._timestamp.month,
                        day=self._timestamp.day,
                        hour=0,
                        minute=0,
                        tzinfo=timezone.utc,
                    ),
                    end=datetime(
                        year=self._timestamp.year,
                        month=self._timestamp.month,
                        day=self._timestamp.day,
                        hour=23,
                        minute=30,
                        tzinfo=timezone.utc,
                    ),
                )
                .sort_values()[: self._n_dsr_intervals]
                .index
            )

        # Get air temp and PV output
        pv_and_temp_output = self._components["PVandTemp"].dispatch(self._timestamp, {})

        outside_air_temp = pv_and_temp_output["outdoor_temp_C"]

        # Get inflexible load
        house_load_output = self._components["HouseLoad"].dispatch(self._timestamp, {})

        house_load_kw = house_load_output["usage_kW"]

        if self._timestamp in self._dsr_schedule[self._timestamp.date()]:
            room_setpoint_temperature_C = self.input_params[
                "room_setpoint_temperature_flex_C"
            ]
            dsr_active = True
        else:
            room_setpoint_temperature_C = self.input_params[
                "room_setpoint_temperature_C"
            ]
            dsr_active = False

        # Get heat pump
        heat_pump_output = self._components["HeatPump"].dispatch(
            self._timestamp,
            {
                "dhw_supply_active": False,
                "room_setpoint_temperature_C": room_setpoint_temperature_C,
                "outdoor_air_temperature_C": outside_air_temp,
            },
        )
        heat_pump_power_kw = heat_pump_output["power_usage_kW"]

        grid_control = {"grid_requested_kw": -(heat_pump_power_kw + house_load_kw)}

        grid_output = self._components["Grid"].dispatch(self._timestamp, grid_control)

        return (
            {"timestamp": str(self._timestamp), "dsr_active": dsr_active}
            | pv_and_temp_output
            | house_load_output
            | heat_pump_output
            | grid_output
            | grid_price
        )
