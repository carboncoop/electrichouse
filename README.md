# Electric House Model

## About

This is a proof of concept implementation of a 'time stepping' energy model aimed at domestic and community scale energy systems in a UK context. The model advances one time step to the next (30 minutes here but this could be modified) 'dispatching' the different energy appliances (PV, heat pumps, EV) in the system and evaluating the resulting contribution to the energy balance (power used / generated) of the different energy appliances subject to both internal and system control. The operation of the system is also subject to certain technical and economic constraints. Once the model has finished time stepping the results can be used to evaluate cost functions over which the model input parameters could be optimised.

The purpose of the model was to help answer questions which require consideration of the impact of control and other time varying effects. These effects become more significant as more appliances are integrated/coupled together and operated to achieve a variety of objectives (saving money , increasing self-consumption). Examples which are difficult to study with parameterised models would be explicit demand side response, the impact of time of use tariffs, dynamic heat models, and alternative flexible load and battery scheduling algorithms.

## Installation

A poetry project file (`pyproject.toml`) is provided along with a `requirements.txt` (and `requirements-dev.txt`) generated from `poetry.lock`. For example, to install in a virtualenv

```
pip install -r requirements.txt
```

## Using the model

To use the model you need to either use one of the provided <>Controller components or implement your own. A <>Controller initialises and then dispatches a number of components at each time step.

If creating your own Controller you can inherit from BaseController and implement the `__post_init__` and `_time_step` methods. The `__post_init__` method is the normal dataclasses[] custom initialisation method which is run after the initialisation of the dataclass. The `_time_step` method performs the calculations in each time step. It should probably return the outputs from the time step for further analysis.

To time step the controller consume it as an iterator in a for loop. This will call the \_time_step method once for each iteration.

Basic components for domestic inflexible load, PV, weather data, heat pump, battery, EV, and DHW are provided.

See the provided notebooks for examples of how to use the model and analyse the results using Pandas.
